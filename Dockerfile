FROM openjdk:17
COPY target/vaski-moneyservice.jar vaski-moneyservice.jar
ENTRYPOINT ["java", "-jar", "/vaski-moneyservice.jar"]

# P_03_VASki_Money_service

Money service - backend for VA Ski project.

VA Ski - Virtual Assistant Ski

## Getting started

### How to start application in kubernetes:

- [ ] Download/Clone files from [kubernetes](https://gitlab.com/vix98sd/p_03_vaski_money_service/-/tree/development/kubernetes) directory
- [ ] Change working directory to `./kubernetes` directory and apply following command: `kubectl apply -f .`
- [ ] To test money service backend you can use [Postman](https://www.postman.com/) and send requests to [localhost:30101](http://localhost:30101) or you can try with [money service frontend](https://gitlab.com/vix98sd/p_03_vaski_money_service_frontend)
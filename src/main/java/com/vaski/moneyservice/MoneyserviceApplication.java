package com.vaski.moneyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoneyserviceApplication.class, args);
    }

}

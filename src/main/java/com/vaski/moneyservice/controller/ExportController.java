package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/export")
@RequiredArgsConstructor
public class ExportController {

    private final WalletService walletService;
    private final TransactionService transactionService;

    @GetMapping("{walletId}")
    public WalletInfoDto exportDataForWallet(@PathVariable("walletId") Long walletId){
        return WalletInfoDto.mapToDto(walletService.findWalletById(walletId), transactionService);
    }

    @GetMapping
    public List<WalletInfoDto> exportDataForAllWallets(){
        List<Wallet> wallets = walletService.findAllWallets();
        return wallets.stream()
                .map(wallet -> WalletInfoDto.mapToDto(wallet, transactionService))
                .collect(Collectors.toList());
    }
}

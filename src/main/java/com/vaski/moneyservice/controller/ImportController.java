package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/import")
@RequiredArgsConstructor
public class ImportController {

    private final WalletService walletService;
    private final TransactionService transactionService;

    @PostMapping
    public void importDataForWallets(@RequestBody List<WalletInfoDto> walletsInfoDto){
        Map<Wallet, List<Transaction>> walletsInfo = walletsInfoDto.stream()
                .map(WalletInfoDto::mapFromDto)
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        walletService.importWallets(walletsInfo);
        transactionService.importTransactions(walletsInfo);
    }
}
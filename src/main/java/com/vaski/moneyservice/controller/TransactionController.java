package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.TransactionDto;
import com.vaski.moneyservice.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/transactions")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;

    @PostMapping("internal/{outgoingWalletId}/{incomingWalletId}")
    public List<TransactionDto> makeNewInternalTransaction(@RequestBody TransactionDto transactionDto,
                                                           @PathVariable("outgoingWalletId") Long outgoingWalletId,
                                                           @PathVariable("incomingWalletId") Long incomingWalletId
    ) {
        return transactionService.makeNewInternalTransaction(TransactionDto.mapFromDto(transactionDto), outgoingWalletId, incomingWalletId)
                .stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("earning")
    public List<TransactionDto> makeNewEarningTransaction(@RequestBody TransactionDto transactionDto) {
        return transactionService.makeNewEarningTransaction(TransactionDto.mapFromDto(transactionDto))
                .stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("spending/{outgoingWalletId}")
    public List<TransactionDto> makeNewSpendingTransaction(
            @RequestBody TransactionDto transactionDto,
            @PathVariable("outgoingWalletId") Long outgoingWalletId) {
        return transactionService.makeNewSpendingTransaction(TransactionDto.mapFromDto(transactionDto), outgoingWalletId)
                .stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("{transactionId}")
    public void deleteTransaction(@PathVariable("transactionId") Long transactionId){
        transactionService.deleteTransaction(transactionId);
    }
    
}

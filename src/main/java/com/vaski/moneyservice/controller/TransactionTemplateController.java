package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.TransactionDto;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionTemplate;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.TransactionTemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/transactions/template")
@RequiredArgsConstructor
public class TransactionTemplateController {

    private final TransactionService transactionService;
    private final TransactionTemplateService transactionTemplateService;

    @GetMapping
    public List<TransactionTemplate> getAllTransactionTemplates() {
        return transactionTemplateService.getAllTemplates();
    }

    @DeleteMapping("{transactionTemplateId}")
    public void deleteTransactionTemplate(@PathVariable("transactionTemplateId") Long transactionTemplateId) {
        transactionTemplateService.deleteTemplateIfExists(transactionTemplateId);
    }

    @PostMapping("internal/{outgoingWalletId}/{incomingWalletId}")
    public List<TransactionDto> makeNewInternalTemplateTransaction(@RequestBody TransactionDto transactionDto,
                                                                   @PathVariable("outgoingWalletId") Long outgoingWalletId,
                                                                   @PathVariable("incomingWalletId") Long incomingWalletId
    ) {
        List<Transaction> transactions = transactionService.makeNewInternalTransaction(TransactionDto.mapFromDto(transactionDto), outgoingWalletId, incomingWalletId);
        transactionTemplateService.createNewTemplate(transactions.get(0));
        return transactions.stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("earning")
    public List<TransactionDto> makeNewEarningTemplateTransaction(@RequestBody TransactionDto transactionDto) {
        List<Transaction> transactions = transactionService.makeNewEarningTransaction(TransactionDto.mapFromDto(transactionDto));
        transactionTemplateService.createNewTemplate(transactions.get(0));
        return transactions.stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("spending/{outgoingWalletId}")
    public List<TransactionDto> makeNewSpendingTemplateTransaction(
            @RequestBody TransactionDto transactionDto,
            @PathVariable("outgoingWalletId") Long outgoingWalletId
    ) {
        List<Transaction> transactions = transactionService.makeNewSpendingTransaction(TransactionDto.mapFromDto(transactionDto), outgoingWalletId);
        transactionTemplateService.createNewTemplate(transactions.get(0));
        return transactions.stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
    }
}

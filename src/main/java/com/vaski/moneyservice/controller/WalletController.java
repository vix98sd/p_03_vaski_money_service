package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletDto;
import com.vaski.moneyservice.domain.dto.WalletInfoAllDto;
import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.domain.models.WalletCategory;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/wallets")
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;
    private final TransactionService transactionService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createNewWallet(@RequestBody WalletDto walletDto) {
        walletService.createNewWallet(WalletDto.mapFromDto(walletDto));
    }

    @GetMapping("init")
    public List<WalletDto> initializeAccount() {
        Wallet earning = new Wallet(null, "Earning", WalletCategory.SYSTEM);
        Wallet spending = new Wallet(null, "Spending", WalletCategory.SYSTEM);
        Wallet unallocated = new Wallet(null, "Unallocated", WalletCategory.SAVINGS);
        return List.of(
                WalletDto.mapToDto(walletService.createNewWallet(earning), transactionService),
                WalletDto.mapToDto(walletService.createNewWallet(spending), transactionService),
                WalletDto.mapToDto(walletService.createNewWallet(unallocated), transactionService)
        );
    }

    @GetMapping
    public WalletInfoAllDto findAllWallets() {
        return WalletInfoAllDto.mapToDto(walletService.findAllWallets(), transactionService);
    }

    @GetMapping("{walletId}")
    public WalletInfoDto findWalletById(@PathVariable("walletId") Long walletId) {
        return WalletInfoDto.mapToDto(walletService.findWalletById(walletId), transactionService);
    }

    @DeleteMapping("{walletId}")
    public void deleteWallet(@PathVariable("walletId") Long walletId){
        transactionService.deleteWalletTransactions(walletId);
        walletService.deleteWallet(walletId);
    }
}

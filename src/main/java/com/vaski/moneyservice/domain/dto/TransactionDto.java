package com.vaski.moneyservice.domain.dto;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionCategory;
import com.vaski.moneyservice.domain.models.TransactionType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class TransactionDto {
    private Long id;
    private Long amount;
    private LocalDate date;
    private LocalTime time;
    private String reason;
    private TransactionType type;
    private TransactionCategory category;

    public static TransactionDto mapToDto(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(transaction.getId());
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setDate(transaction.getDate());
        transactionDto.setTime(transaction.getTime());
        transactionDto.setReason(transaction.getReason());
        transactionDto.setType(transaction.getType());
        transactionDto.setCategory(transaction.getCategory());
        return transactionDto;
    }

    public static Transaction mapFromDto(TransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        transaction.setId(transactionDto.getId());
        transaction.setAmount(transactionDto.getAmount());
        transaction.setDate(transactionDto.getDate());
        transaction.setTime(transactionDto.getTime());
        transaction.setReason(transactionDto.getReason());
        transaction.setType(transactionDto.getType());
        transaction.setCategory(transactionDto.getCategory());
        return transaction;
    }
}

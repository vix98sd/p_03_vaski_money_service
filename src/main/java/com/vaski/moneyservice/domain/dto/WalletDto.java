package com.vaski.moneyservice.domain.dto;

import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.domain.models.WalletCategory;
import com.vaski.moneyservice.service.TransactionService;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class WalletDto {
    private Long id;
    private String name;
    private WalletCategory category = WalletCategory.NORMAL;
    private Long balance;

    public static WalletDto mapToDto(Wallet wallet, TransactionService transactionService) {
        WalletDto walletDto = new WalletDto();
        walletDto.id = wallet.getId();
        walletDto.name = wallet.getName();
        walletDto.category = wallet.getCategory();
        walletDto.balance = transactionService.getWalletTransactionBalance(wallet);
        return walletDto;
    }

    public static Wallet mapFromDto(WalletDto walletDto) {
        Wallet wallet = new Wallet();
        wallet.setId(walletDto.getId());
        wallet.setName(walletDto.getName());
        wallet.setCategory(walletDto.getCategory());
        return wallet;
    }
}

package com.vaski.moneyservice.domain.dto;

import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class WalletInfoAllDto {
    private List<WalletDto> wallets;

    public static WalletInfoAllDto mapToDto(List<Wallet> wallets, TransactionService transactionService) {
        WalletInfoAllDto walletInfoAllDto = new WalletInfoAllDto();
        walletInfoAllDto.wallets = wallets.stream()
                .map(wallet -> WalletDto.mapToDto(wallet, transactionService))
                .collect(Collectors.toList());
        return walletInfoAllDto;
    }
}

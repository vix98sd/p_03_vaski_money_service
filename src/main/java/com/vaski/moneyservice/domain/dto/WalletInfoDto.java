package com.vaski.moneyservice.domain.dto;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class WalletInfoDto {
    private WalletDto wallet;
    private List<TransactionDto> walletTransactions;

    public static WalletInfoDto mapToDto(Wallet wallet, TransactionService transactionService) {
        WalletInfoDto walletInfoDto = new WalletInfoDto();
        walletInfoDto.wallet = WalletDto.mapToDto(wallet, transactionService);
        walletInfoDto.walletTransactions = transactionService.findWalletTransactions(wallet).stream()
                .map(TransactionDto::mapToDto)
                .collect(Collectors.toList());
        return walletInfoDto;
    }

    public static Entry<Wallet, List<Transaction>> mapFromDto(WalletInfoDto walletInfoDto) {
        Wallet wallet = WalletDto.mapFromDto(walletInfoDto.wallet);
        List<Transaction> transactions = walletInfoDto.walletTransactions.stream()
                .map(TransactionDto::mapFromDto)
                .peek(transaction -> transaction.setAssociatedWallet(wallet))
                .collect(Collectors.toList());
        return Map.entry(wallet, transactions);
    }
}

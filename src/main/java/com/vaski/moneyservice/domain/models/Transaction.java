package com.vaski.moneyservice.domain.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

import static javax.persistence.GenerationType.SEQUENCE;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "Transaction")
@Table(name = "transactions")
public class Transaction {
    @Id
    @SequenceGenerator(
            name = "transactions_sequence",
            sequenceName = "transactions_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "transactions_sequence"
    )
    private Long id;
    @ManyToOne
    @JoinColumn(name = "from_wallet_id", referencedColumnName = "id")
    private Wallet associatedWallet;
    private Long amount;
    private LocalDate date;
    private LocalTime time;
    private String reason;
    private TransactionType type;
    private TransactionCategory category;
    @OneToOne
    @JoinColumn(name = "transaction_pair_id", referencedColumnName = "id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Transaction transactionPair;
    private boolean isArchived = false;

    public Transaction(Long id, Wallet associatedWallet, Long amount, LocalDate date, LocalTime time, String reason, TransactionType type, TransactionCategory category) {
        this.id = id;
        this.associatedWallet = associatedWallet;
        this.amount = amount;
        this.date = date;
        this.time = time;
        this.reason = reason;
        this.type = type;
        this.category = category;
    }

    public Long getEvaluatedAmount() {
        return this.type.evaluate(this.amount);
    }

}

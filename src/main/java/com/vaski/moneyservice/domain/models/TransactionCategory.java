package com.vaski.moneyservice.domain.models;

public enum TransactionCategory {
    EARNING,
    SPENDING,
    INTERNAL
}

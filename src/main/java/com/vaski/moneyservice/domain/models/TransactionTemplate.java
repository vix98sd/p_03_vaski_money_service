package com.vaski.moneyservice.domain.models;

import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "TransactionTemplate")
@Table(name = "transaction_templates")
public class TransactionTemplate {
    @Id
    @SequenceGenerator(
            name = "transaction_templates_sequence",
            sequenceName = "transaction_templates_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "transaction_templates_sequence"
    )
    public Long id;
    @OneToOne
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    public Transaction transaction;

    public TransactionTemplate(Transaction transaction){
        this.transaction = transaction;
    }
}

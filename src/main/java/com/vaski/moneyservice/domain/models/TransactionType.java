package com.vaski.moneyservice.domain.models;

import java.util.function.LongUnaryOperator;

public enum TransactionType {
    INFLOW(x -> x),
    OUTFLOW(x -> -1 * x);

    private final LongUnaryOperator operator;

    TransactionType(LongUnaryOperator operator) {
        this.operator = operator;
    }

    public long evaluate(long x) {
        return operator.applyAsLong(x);
    }
}

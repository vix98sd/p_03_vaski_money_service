package com.vaski.moneyservice.domain.models;

import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "Wallet")
@Table(name = "wallets")
public class Wallet {
    @Id
    @SequenceGenerator(
            name = "wallets_sequence",
            sequenceName = "wallets_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "wallets_sequence"
    )
    private Long id;
    private String name;
    private WalletCategory category = WalletCategory.NORMAL;
    private boolean isArchived = false;

    public Wallet(Long id, String name, WalletCategory category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }
}

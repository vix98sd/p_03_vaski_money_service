package com.vaski.moneyservice.domain.models;

public enum WalletCategory {
    NORMAL,
    SAVINGS,
    SYSTEM
}

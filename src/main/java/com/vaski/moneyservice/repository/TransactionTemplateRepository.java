package com.vaski.moneyservice.repository;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionTemplateRepository extends JpaRepository<TransactionTemplate, Long> {
    Optional<TransactionTemplate> findByTransaction(Transaction transactionToBeDeleted);
}

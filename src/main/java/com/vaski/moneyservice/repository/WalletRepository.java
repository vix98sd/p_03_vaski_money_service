package com.vaski.moneyservice.repository;

import com.vaski.moneyservice.domain.models.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    Optional<Wallet> findWalletByName(String name);

    boolean existsByNameAndIsArchived(String name, boolean isArchived);

    List<Wallet> findAllByIsArchived(boolean isArchived);
}

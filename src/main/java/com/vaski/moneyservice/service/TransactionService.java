package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionCategory;
import com.vaski.moneyservice.domain.models.Wallet;

import java.util.List;
import java.util.Map;

public interface TransactionService {

    List<Transaction> makeNewTransaction(Transaction transaction,
                                         TransactionCategory transactionCategory,
                                         Wallet outgoingWallet,
                                         Wallet incomingWallet);

    List<Transaction> makeNewInternalTransaction(Transaction transaction, Long outgoingWalletId, Long incomingWalletId);

    List<Transaction> makeNewEarningTransaction(Transaction transaction);

    List<Transaction> makeNewSpendingTransaction(Transaction transaction, Long outgoingWalletId);

    Transaction getTransaction(Long transactionId);

    Transaction getTransactionPair(Long transactionId);

    Long getWalletTransactionBalance(Wallet wallet);

    List<Transaction> findWalletTransactions(Wallet wallet);

    void importTransactions(Map<Wallet, List<Transaction>> walletsInfo);

    void deleteTransaction(Long transactionId);

    void deleteWalletTransactions(Long walletId);

    void archiveTransaction(Transaction transactionToBeArchived);

    void archiveWalletTransactions(List<Transaction> walletTransactionsToBeArchived);

}

package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionCategory;
import com.vaski.moneyservice.domain.models.TransactionType;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.vaski.moneyservice.domain.models.TransactionCategory.*;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final WalletService walletService;
    private final TransactionTemplateService transactionTemplateService;

    @Override
    public List<Transaction> makeNewTransaction(Transaction transaction, TransactionCategory transactionCategory, Wallet outgoingWallet, Wallet incomingWallet) {
        Transaction outgoingTransaction = new Transaction(
                null,
                outgoingWallet,
                transaction.getAmount(),
                transaction.getDate() != null ? transaction.getDate() : LocalDate.now(),
                transaction.getTime() != null ? transaction.getTime() : LocalTime.of(12, 0),
                transaction.getReason() != null ? transaction.getReason() : "To" + incomingWallet.getName(),
                TransactionType.OUTFLOW,
                transactionCategory
        );
        Transaction incomingTransaction = new Transaction(
                null,
                incomingWallet,
                transaction.getAmount(),
                outgoingTransaction.getDate(),
                outgoingTransaction.getTime(),
                transaction.getReason() != null ? transaction.getReason() : "From" + outgoingWallet.getName(),
                TransactionType.INFLOW,
                transactionCategory
        );
        outgoingTransaction.setTransactionPair(incomingTransaction);
        incomingTransaction.setTransactionPair(outgoingTransaction);
        return transactionRepository.saveAll(List.of(outgoingTransaction, incomingTransaction));
    }

    @Override
    public List<Transaction> makeNewInternalTransaction(Transaction transaction, Long outgoingWalletId, Long incomingWalletId) {
        Wallet outgoingWallet = walletService.findWalletById(outgoingWalletId);
        Wallet incomingWallet = walletService.findWalletById(incomingWalletId);
        transaction.setReason(null);
        return this.makeNewTransaction(transaction, INTERNAL, outgoingWallet, incomingWallet);
    }

    @Override
    public List<Transaction> makeNewEarningTransaction(Transaction transaction) {
        Wallet outgoingWallet = walletService.findWalletByName("Earning");
        Wallet incomingWallet = walletService.findWalletByName("Unallocated");
        return this.makeNewTransaction(transaction, EARNING, outgoingWallet, incomingWallet);
    }

    @Override
    public List<Transaction> makeNewSpendingTransaction(Transaction transaction, Long outgoingWalletId) {
        Wallet outgoingWallet = walletService.findWalletById(outgoingWalletId);
        Wallet incomingWallet = walletService.findWalletByName("Spending");
        return this.makeNewTransaction(transaction, SPENDING, outgoingWallet, incomingWallet);
    }

    @Override
    public Transaction getTransaction(Long transactionId) {
        return transactionRepository.findById(transactionId).orElseThrow(() ->
                new IllegalStateException("transaction with id " + transactionId + " was not found"));
    }

    @Override
    public Transaction getTransactionPair(Long transactionId) {
        Transaction transaction = this.getTransaction(transactionId);
        return transaction.getTransactionPair();
    }

    @Override
    public Long getWalletTransactionBalance(Wallet wallet) {
        return this.findWalletTransactions(wallet)
                .stream()
                .mapToLong(Transaction::getEvaluatedAmount)
                .sum();
    }

    @Override
    public List<Transaction> findWalletTransactions(Wallet wallet) {
        return transactionRepository.findAllByAssociatedWalletAndIsArchived(wallet, false);
    }

    @Override
    public void importTransactions(Map<Wallet, List<Transaction>> walletsInfo) {
        List<Transaction> newTransactionsToBeImported = walletsInfo.entrySet().stream()
                .map(walletInfo -> {
                    Transaction lastWalletTransaction = this.findLastWalletTransaction(walletInfo.getKey());
                    LocalDateTime lastTransactionDateTime = LocalDateTime.of(lastWalletTransaction.getDate(), lastWalletTransaction.getTime());
                    return walletInfo.getValue().stream()
                            .filter(transaction -> {
                                LocalDateTime transactionDateTime = LocalDateTime.of(transaction.getDate(), transaction.getTime());
                                return lastTransactionDateTime.isBefore(transactionDateTime);
                            })
                            .peek(transaction -> transaction.setAssociatedWallet(walletInfo.getKey()))
                            .peek(transaction -> transaction.setId(null))
                            .collect(Collectors.toList());
                })
                .flatMap(List::stream)
                .collect(Collectors.toList());

        transactionRepository.saveAll(newTransactionsToBeImported);
    }

    @Override
    public void deleteTransaction(Long transactionId) {
        Transaction transactionToBeDeleted = this.getTransaction(transactionId);
        transactionTemplateService.deleteTemplateIfExists(transactionToBeDeleted);
        transactionTemplateService.deleteTemplateIfExists(transactionToBeDeleted.getTransactionPair());
        this.archiveTransaction(transactionToBeDeleted);
    }

    @Override
    public void deleteWalletTransactions(Long walletId) {
        Wallet wallet = walletService.findWalletById(walletId);
        List<Transaction> walletTransactionsToBeDeleted = this.findWalletTransactions(wallet);
        walletTransactionsToBeDeleted.forEach(transactionTemplateService::deleteTemplateIfExists);
        this.archiveWalletTransactions(walletTransactionsToBeDeleted);
    }

    @Override
    public void archiveTransaction(Transaction transactionToBeArchived) {
        transactionToBeArchived.setArchived(true);
        transactionToBeArchived.getTransactionPair().setArchived(true);
        transactionRepository.save(transactionToBeArchived);
    }

    @Override
    public void archiveWalletTransactions(List<Transaction> walletTransactionsToBeArchived) {
        walletTransactionsToBeArchived.forEach(transaction -> transaction.setArchived(true));
        transactionRepository.saveAll(walletTransactionsToBeArchived);
    }

    public Transaction findLastWalletTransaction(Wallet wallet) {
        return this.findWalletTransactions(wallet).stream()
                .max(Comparator.comparing(Transaction::getDate).thenComparing(Transaction::getTime))
                .orElse(new Transaction(null, null, null, LocalDate.of(1900, 1, 1), LocalTime.of(0, 0), null, null, null));
    }


}

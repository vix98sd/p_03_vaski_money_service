package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionTemplate;

import java.util.List;

public interface TransactionTemplateService {
    void createNewTemplate(Transaction transactionTemplate);

    List<TransactionTemplate> getAllTemplates();

    TransactionTemplate getTemplateById(Long transactionTemplateId);

    TransactionTemplate getTemplateByTransaction(Transaction transaction);

    void deleteTemplateIfExists(Transaction transactionToBeDeleted);

    void deleteTemplateIfExists(Long transactionTemplateId);
}

package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionTemplate;
import com.vaski.moneyservice.repository.TransactionTemplateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionTemplateServiceImpl implements TransactionTemplateService {

    private final TransactionTemplateRepository transactionTemplateRepository;

    @Override
    public void createNewTemplate(Transaction transaction) {
        transactionTemplateRepository.save(new TransactionTemplate(transaction));
    }

    @Override
    public List<TransactionTemplate> getAllTemplates() {
        return transactionTemplateRepository.findAll();
    }

    @Override
    public TransactionTemplate getTemplateById(Long transactionTemplateId) {
        return transactionTemplateRepository.findById(transactionTemplateId).orElseThrow(() ->
                new IllegalStateException("transaction template with id " + transactionTemplateId + " was not found" ));
    }

    @Override
    public TransactionTemplate getTemplateByTransaction(Transaction transaction){
        return transactionTemplateRepository.findByTransaction(transaction).orElse(null);
    }

    @Override
    public void deleteTemplateIfExists(Transaction transactionToBeDeleted) {
        TransactionTemplate transactionTemplateToBeDeleted = this.getTemplateByTransaction(transactionToBeDeleted);
        if(transactionTemplateToBeDeleted != null){
            transactionTemplateRepository.delete(transactionTemplateToBeDeleted);
        }
    }

    @Override
    public void deleteTemplateIfExists(Long transactionTemplateId) {
        transactionTemplateRepository.deleteById(transactionTemplateId);
    }
}

package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.Wallet;

import java.util.List;
import java.util.Map;

public interface WalletService {
    Wallet createNewWallet(Wallet wallet);

    Wallet findWalletById(Long walletId);

    List<Wallet> findAllWallets();

    Wallet findWalletByName(String earning);

    void importWallets(Map<Wallet, List<Transaction>> walletsInfo);

    void deleteWallet(Long walletId);

    void archiveWallet(Long walletId);
}

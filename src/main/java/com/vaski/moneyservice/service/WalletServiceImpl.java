package com.vaski.moneyservice.service;

import com.vaski.moneyservice.domain.exception.RecordAlreadyExistException;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.domain.models.WalletCategory;
import com.vaski.moneyservice.repository.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    @Override
    public Wallet createNewWallet(Wallet wallet) {
        if (walletRepository.existsByNameAndIsArchived(wallet.getName(), false)) {
            throw new RecordAlreadyExistException(String.format("wallet with name %s already exists", wallet.getName()));
        }
        return walletRepository.save(wallet);
    }

    @Override
    public Wallet findWalletById(Long walletId) {
        return walletRepository.findById(walletId).orElseThrow(() ->
                new IllegalStateException("wallet with id " + walletId + " was not found"));
    }

    @Override
    public List<Wallet> findAllWallets() {
        return walletRepository.findAllByIsArchived(false).stream()
                .filter(wallet -> !wallet.getCategory().equals(WalletCategory.SYSTEM))
                .collect(Collectors.toList());
    }

    @Override
    public Wallet findWalletByName(String walletName) {
        return walletRepository.findWalletByName(walletName).orElseThrow(() ->
                new IllegalStateException("wallet with name " + walletName + " was not found"));
    }

    @Override
    public void importWallets(Map<Wallet, List<Transaction>> walletsInfo) {
        List<Wallet> previousWallets = this.findAllWallets();
        walletRepository.saveAll(
                walletsInfo.keySet().stream()
                        .filter(wallet -> previousWallets.stream()
                                .noneMatch(previousWallet -> previousWallet.getName().equals(wallet.getName())))
                        .peek(wallet -> wallet.setId(null))
                        .collect(Collectors.toList()));
    }

    @Override
    public void deleteWallet(Long walletId) {
        this.archiveWallet(walletId);
    }

    @Override
    public void archiveWallet(Long walletId) {
        Wallet walletToBeArchived = this.findWalletById(walletId);
        if (walletToBeArchived.getCategory() == WalletCategory.SYSTEM) {
            throw new IllegalStateException("you can not delete system wallet");
        }
        walletToBeArchived.setArchived(true);
        walletRepository.save(walletToBeArchived);
    }
}

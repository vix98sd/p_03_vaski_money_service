package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.*;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ExportControllerTest {

    @Mock
    private WalletService walletService;
    @Mock
    private TransactionService transactionService;

    private ExportController exportController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        exportController = new ExportController(walletService, transactionService);
    }

    @Test
    void exportDataForWallet() {
        // Given
        Wallet wallet = MockDataProvider.getNormalWallet(1L);
        Transaction transaction1 = new Transaction(1L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(2L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Mockito.when(transactionService.findWalletTransactions(wallet))
                .thenReturn(List.of(transaction1, transaction2, transaction3));
        Mockito.when(transactionService.getWalletTransactionBalance(wallet)).thenReturn(1000L);
        WalletInfoDto expectedWalletInfoDto = WalletInfoDto.mapToDto(wallet, transactionService);
        Mockito.when(walletService.findWalletById(wallet.getId())).thenReturn(wallet);
        // When
        WalletInfoDto actualWalletInfoDto = exportController.exportDataForWallet(wallet.getId());
        // Then
        assertEquals(expectedWalletInfoDto, actualWalletInfoDto);
    }

    @Test
    void exportDataForAllWallets() {
        // Given
        Wallet wallet1 = MockDataProvider.getNormalWallet(1L);
        Wallet wallet2 = MockDataProvider.getNormalWallet(2L);
        Wallet wallet3 = MockDataProvider.getNormalWallet(3L);
        List<Wallet> wallets = List.of(wallet1, wallet2, wallet3);
        Mockito.when(walletService.findAllWallets()).thenReturn(wallets);
        Transaction transaction1 = new Transaction(1L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(2L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction4 = new Transaction(4L, wallet2, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction5 = new Transaction(5L, wallet3, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<Transaction> allTransactions = List.of(transaction1, transaction2, transaction3, transaction4, transaction5);
        Mockito.when(transactionService.getWalletTransactionBalance(Mockito.any(Wallet.class))).thenReturn(1000L);
        Mockito.when(transactionService.findWalletTransactions(Mockito.any(Wallet.class)))
                .thenAnswer(i -> allTransactions.stream().filter(transaction -> transaction.getAssociatedWallet().equals(i.getArgument(0))).collect(Collectors.toList()));
        List<WalletInfoDto> expectedWalletInfoDtos = wallets.stream().map(wallet -> WalletInfoDto.mapToDto(wallet, transactionService)).collect(Collectors.toList());
        // When
        List<WalletInfoDto> actualWalletInfoDtos = exportController.exportDataForAllWallets();
        // Then
        assertEquals(expectedWalletInfoDtos, actualWalletInfoDtos);
    }
}
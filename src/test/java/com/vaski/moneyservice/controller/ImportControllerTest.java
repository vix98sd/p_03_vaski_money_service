package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.domain.models.*;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

class ImportControllerTest {

    @Mock
    private WalletService walletService;
    @Mock
    private TransactionService transactionService;

    private ImportController importController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        importController = new ImportController(walletService, transactionService);
    }

    @Test
    void importDataForWallets() {
        // Given
        Wallet wallet1 = new Wallet(1L, "Wallet 1", WalletCategory.NORMAL);
        Wallet wallet2 = new Wallet(2L, "Wallet 2", WalletCategory.NORMAL);
        Wallet wallet3 = new Wallet(3L, "Wallet 3", WalletCategory.NORMAL);
        List<Wallet> wallets = List.of(wallet1, wallet2, wallet3);
        Mockito.when(walletService.findAllWallets()).thenReturn(wallets);
        Transaction transaction1 = new Transaction(1L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(2L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, wallet1, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction4 = new Transaction(4L, wallet2, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction5 = new Transaction(5L, wallet3, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<Transaction> allTransactions = List.of(transaction1, transaction2, transaction3, transaction4, transaction5);
        Mockito.when(transactionService.getWalletTransactionBalance(Mockito.any(Wallet.class))).thenReturn(1000L);
        Mockito.when(transactionService.findWalletTransactions(Mockito.any(Wallet.class)))
                .thenAnswer(i -> allTransactions.stream().filter(transaction -> transaction.getAssociatedWallet().equals(i.getArgument(0))).collect(Collectors.toList()));
        WalletInfoDto walletInfoDto1 = WalletInfoDto.mapToDto(wallet1, transactionService);
        WalletInfoDto walletInfoDto2 = WalletInfoDto.mapToDto(wallet2, transactionService);
        WalletInfoDto walletInfoDto3 = WalletInfoDto.mapToDto(wallet3, transactionService);
        List<WalletInfoDto> walletsInfoDto = List.of(walletInfoDto1, walletInfoDto2, walletInfoDto3);
        Mockito.doNothing().when(walletService).importWallets(Mockito.any());
        Mockito.doNothing().when(transactionService).importTransactions(Mockito.any());
        // When
        importController.importDataForWallets(walletsInfoDto);
        // Then
        Mockito.verify(walletService, Mockito.times(1))
                        .importWallets(Mockito.any());
        Mockito.verify(transactionService, Mockito.times(1))
                        .importTransactions(Mockito.any());
    }
}
package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.TransactionDto;
import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionCategory;
import com.vaski.moneyservice.domain.models.TransactionType;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransactionControllerTest {

    @Mock
    private TransactionService transactionService;

    private TransactionController transactionController;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        transactionController = new TransactionController(transactionService);
    }

    @Test
    void makeNewInternalTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(null, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transactionToBeCreatedPair = new Transaction(null, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());
        Mockito.when(transactionService.makeNewInternalTransaction(transactionToBeCreated, outgoingWallet.getId(), incomingWallet.getId()))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));
        // When
        transactionController.makeNewInternalTransaction(transactionToBeCreatedDto, outgoingWallet.getId(), incomingWallet.getId());
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
    }

    @Test
    void makeNewEarningTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(null, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.EARNING);
        Transaction transactionToBeCreatedPair = new Transaction(null, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());
        Mockito.when(transactionService.makeNewEarningTransaction(transactionToBeCreated))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));
        // When
        transactionController.makeNewEarningTransaction(transactionToBeCreatedDto);
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
    }

    @Test
    void makeNewSpendingTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(null, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.SPENDING);
        Transaction transactionToBeCreatedPair = new Transaction(null, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());
        Mockito.when(transactionService.makeNewSpendingTransaction(transactionToBeCreated, outgoingWallet.getId()))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));
        // When
        transactionController.makeNewSpendingTransaction(transactionToBeCreatedDto, outgoingWallet.getId());
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
    }

    @Test
    void deleteTransaction() {
        // Given
        Long transactionId = 1L;
        Mockito.doNothing().when(transactionService).deleteTransaction(transactionId);
        // When
        transactionController.deleteTransaction(transactionId);
        // Then
        Mockito.verify(transactionService, Mockito.times(1))
                        .deleteTransaction(transactionId);
    }
}
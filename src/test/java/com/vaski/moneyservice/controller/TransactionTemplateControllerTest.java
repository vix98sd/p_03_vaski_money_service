package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.TransactionDto;
import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.*;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.TransactionTemplateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTemplateControllerTest {

    @Mock
    private TransactionService transactionService;
    @Mock
    private TransactionTemplateService transactionTemplateService;

    private TransactionTemplateController transactionTemplateController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transactionTemplateController = new TransactionTemplateController(transactionService, transactionTemplateService);
    }

    @Test
    void getAllTransactionTemplates() {
        // Given
        Wallet wallet = MockDataProvider.getNormalWallet(1L);
        Transaction transaction1 = new Transaction(1L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(2L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, wallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        TransactionTemplate transactionTemplate1 = new TransactionTemplate(1L, transaction1);
        TransactionTemplate transactionTemplate2 = new TransactionTemplate(2L, transaction2);
        TransactionTemplate transactionTemplate3 = new TransactionTemplate(3L, transaction3);
        List<TransactionTemplate> expectedTransactionTemplates = List.of(transactionTemplate1, transactionTemplate2, transactionTemplate3);
        Mockito.when(transactionTemplateService.getAllTemplates())
                .thenReturn(expectedTransactionTemplates);
        // When
        List<TransactionTemplate> actualTransactionTemplates = transactionTemplateController.getAllTransactionTemplates();
        // Then
        assertEquals(expectedTransactionTemplates, actualTransactionTemplates);
    }

    @Test
    void deleteTransactionTemplate() {
        // Given
        Long transactionTemplateId = 1L;
        Mockito.doNothing().when(transactionTemplateService).deleteTemplateIfExists(transactionTemplateId);
        // When
        transactionTemplateController.deleteTransactionTemplate(transactionTemplateId);
        // Then
        Mockito.verify(transactionTemplateService, Mockito.times(1))
                .deleteTemplateIfExists(transactionTemplateId);
    }

    @Test
    void makeNewInternalTemplateTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(1L, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transactionToBeCreatedPair = new Transaction(2L, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());

        Mockito.when(transactionService.makeNewInternalTransaction(Mockito.any(Transaction.class), Mockito.anyLong(), Mockito.anyLong()))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));

        Mockito.doNothing().when(transactionTemplateService).createNewTemplate(transactionToBeCreated);
        // When
        transactionTemplateController.makeNewInternalTemplateTransaction(transactionToBeCreatedDto, outgoingWallet.getId(), incomingWallet.getId());
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
        Mockito.verify(transactionTemplateService, Mockito.times(1))
                .createNewTemplate(transactionToBeCreated);
    }

    @Test
    void makeNewEarningTemplateTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(1L, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.EARNING);
        Transaction transactionToBeCreatedPair = new Transaction(2L, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());
        Mockito.when(transactionService.makeNewEarningTransaction(Mockito.any(Transaction.class)))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));
        Mockito.doNothing().when(transactionTemplateService).createNewTemplate(transactionToBeCreated);
        // When
        transactionTemplateController.makeNewEarningTemplateTransaction(transactionToBeCreatedDto);
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
        Mockito.verify(transactionTemplateService, Mockito.times(1))
                .createNewTemplate(transactionToBeCreated);
    }

    @Test
    void makeNewSpendingTemplateTransaction() {
        // Given
        Wallet outgoingWallet = MockDataProvider.getNormalWallet(1L);
        Wallet incomingWallet = MockDataProvider.getNormalWallet(2L);
        Transaction transactionToBeCreated = new Transaction(1L, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.SPENDING);
        Transaction transactionToBeCreatedPair = new Transaction(2L, incomingWallet, 1000L, transactionToBeCreated.getDate(), transactionToBeCreated.getTime(), "FromWallet 1", TransactionType.INFLOW, transactionToBeCreated.getCategory());
        TransactionDto transactionToBeCreatedDto = TransactionDto.mapToDto(transactionToBeCreated);
        List<TransactionDto> expectedTransactionsDto = Stream.of(transactionToBeCreated, transactionToBeCreatedPair)
                .map(TransactionDto::mapToDto).collect(Collectors.toList());
        Mockito.when(transactionService.makeNewSpendingTransaction(Mockito.any(Transaction.class), Mockito.anyLong()))
                .thenReturn(List.of(transactionToBeCreated, transactionToBeCreatedPair));
        Mockito.doNothing().when(transactionTemplateService).createNewTemplate(transactionToBeCreated);
        // When
        transactionTemplateController.makeNewSpendingTemplateTransaction(transactionToBeCreatedDto, outgoingWallet.getId());
        // Then
        assertEquals(expectedTransactionsDto, expectedTransactionsDto);
        Mockito.verify(transactionTemplateService, Mockito.times(1))
                .createNewTemplate(transactionToBeCreated);
    }
}
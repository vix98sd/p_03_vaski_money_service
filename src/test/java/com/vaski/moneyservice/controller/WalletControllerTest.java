package com.vaski.moneyservice.controller;

import com.vaski.moneyservice.domain.dto.WalletDto;
import com.vaski.moneyservice.domain.dto.WalletInfoAllDto;
import com.vaski.moneyservice.domain.dto.WalletInfoDto;
import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.service.TransactionService;
import com.vaski.moneyservice.service.WalletService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WalletControllerTest {

    @Mock
    private WalletService walletService;
    @Mock
    private TransactionService transactionService;

    private WalletController walletController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        walletController = new WalletController(walletService, transactionService);
    }

    @Test
    void createNewWallet() {
        // Given
        Wallet walletToBeCreated = MockDataProvider.getNormalWallet(1L);
        Mockito.when(transactionService.getWalletTransactionBalance(walletToBeCreated))
                .thenReturn(1000L);
        WalletDto walletToBeCreatedDto = WalletDto.mapToDto(walletToBeCreated, transactionService);
        Mockito.when(walletService.createNewWallet(Mockito.any(Wallet.class)))
                .thenAnswer(i -> i.getArgument(0));
        // When
        walletController.createNewWallet(walletToBeCreatedDto);
        // Then
        Mockito.verify(walletService, Mockito.times(1))
                .createNewWallet(walletToBeCreated);
    }

    @Test
    void initializeAccount() {
        // Given
        Wallet walletEarning = MockDataProvider.getSystemWallet(null, "Earning");
        Wallet walletSpending = MockDataProvider.getSystemWallet(null, "Spending");
        Wallet walletUnallocated = MockDataProvider.getSavingsWallet(null, "Unallocated");
        Mockito.when(transactionService.getWalletTransactionBalance(Mockito.any(Wallet.class)))
                .thenReturn(1000L);
        List<WalletDto> expectedWalletDtos = List.of(
                WalletDto.mapToDto(walletEarning, transactionService),
                WalletDto.mapToDto(walletSpending, transactionService),
                WalletDto.mapToDto(walletUnallocated, transactionService)
        );
        Mockito.when(walletService.createNewWallet(Mockito.any(Wallet.class)))
                .thenAnswer(i -> i.getArgument(0));
        // When
        List<WalletDto> actualWalletDtos = walletController.initializeAccount();
        // Then
        assertEquals(expectedWalletDtos, actualWalletDtos);
    }

    @Test
    void findAllWallets() {
        // Given
        Wallet wallet1 = MockDataProvider.getNormalWallet(1L);
        Wallet wallet2 = MockDataProvider.getNormalWallet(2L);
        Wallet wallet3 = MockDataProvider.getNormalWallet(3L);
        List<Wallet> wallets = List.of(wallet1, wallet2, wallet3);
        Mockito.when(transactionService.getWalletTransactionBalance(Mockito.any(Wallet.class)))
                .thenReturn(1000L);
        WalletInfoAllDto expectedWalletInfoAllDto = WalletInfoAllDto.mapToDto(wallets, transactionService);
        Mockito.when(walletService.findAllWallets()).thenReturn(wallets);
        // When
        WalletInfoAllDto actualWalletInfoAllDto = walletController.findAllWallets();
        // Then
        assertEquals(expectedWalletInfoAllDto, actualWalletInfoAllDto);
    }

    @Test
    void findWalletById() {
        // Given
        Long walletId = 1L;
        Wallet wallet = MockDataProvider.getNormalWallet(1L);
        Mockito.when(transactionService.getWalletTransactionBalance(Mockito.any(Wallet.class)))
                .thenReturn(1000L);
        WalletInfoDto expectedWalletInfoDto = WalletInfoDto.mapToDto(wallet, transactionService);
        Mockito.when(walletService.findWalletById(walletId)).thenReturn(wallet);
        // When
        WalletInfoDto actualWalletInfoDto = walletController.findWalletById(walletId);
        // Then
        assertEquals(expectedWalletInfoDto, actualWalletInfoDto);
    }

    @Test
    void deleteWallet() {
        // Given
        Long walletId = 1L;
        Mockito.doNothing().when(walletService).deleteWallet(walletId);
        // When
        walletController.deleteWallet(walletId);
        // Then
        Mockito.verify(walletService, Mockito.times(1))
                .deleteWallet(walletId);
    }
}
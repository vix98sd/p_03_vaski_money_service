package com.vaski.moneyservice.dataprovider;

import com.vaski.moneyservice.domain.models.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class MockDataProvider {
    public static Wallet getNormalWallet(Long walletId){
        return new Wallet(walletId, "Wallet " + walletId, WalletCategory.NORMAL);
    }

    public static Wallet getNormalWallet(Long walletId, String walletName){
        return new Wallet(walletId, walletName, WalletCategory.NORMAL);
    }

    public static Wallet getSavingsWallet(Long walletId, String walletName){
        return new Wallet(walletId, walletName, WalletCategory.SAVINGS);
    }

    public static Wallet getSystemWallet(Long walletId, String walletName){
        return new Wallet(walletId, walletName, WalletCategory.SYSTEM);
    }

    public static List<Transaction> getTransactionPairs(Long transactionId, Long transactionPairId){
        Transaction transaction = new Transaction(transactionId, MockDataProvider.getNormalWallet(1L), 1000L, LocalDate.now(), LocalTime.now(), "ToWallet 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transactionPair = new Transaction(transactionPairId, MockDataProvider.getNormalWallet(2L), 1000L, transaction.getDate(), transaction.getTime(), "FromWallet 1", TransactionType.INFLOW, transaction.getCategory());
        transaction.setTransactionPair(transactionPair);
        transactionPair.setTransactionPair(transaction);
        return List.of(transaction, transactionPair);
    }

    public static Transaction getTransactionCopy(Transaction transaction){
        Transaction transactionCopy = new Transaction(transaction.getId(), transaction.getAssociatedWallet(), transaction.getAmount(), transaction.getDate(), transaction.getTime(),transaction.getReason(), transaction.getType(), transaction.getCategory());
        transactionCopy.setTransactionPair(transaction.getTransactionPair());
        return transactionCopy;
    }
}

package com.vaski.moneyservice.service;

import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.TransactionCategory;
import com.vaski.moneyservice.domain.models.TransactionType;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private WalletService walletService;
    @Mock
    private TransactionTemplateService transactionTemplateService;

    private TransactionServiceImpl transactionService;

    private Wallet outgoingWallet;
    private Wallet incomingWallet;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transactionService = new TransactionServiceImpl(
                transactionRepository,
                walletService,
                transactionTemplateService
        );

        outgoingWallet = MockDataProvider.getNormalWallet(1L);
        incomingWallet = MockDataProvider.getNormalWallet(2L);
    }

    @Test
    void importTransactions() {
        // Given
        Transaction transaction1 = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(2L, incomingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.INFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, outgoingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction4 = new Transaction(4L, incomingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.INFLOW, TransactionCategory.INTERNAL);
        Transaction transaction5 = new Transaction(5L, outgoingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction6 = new Transaction(6L, incomingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.INFLOW, TransactionCategory.INTERNAL);
        List<Transaction> wallet1Transactions = List.of(transaction1, transaction3, transaction5);
        List<Transaction> wallet2Transactions = List.of(transaction2, transaction4, transaction6);
        Map<Wallet, List<Transaction>> walletsInfo = Map.of(outgoingWallet, wallet1Transactions, incomingWallet, wallet2Transactions);

        Mockito.when(transactionRepository.findAllByAssociatedWalletAndIsArchived(Mockito.any(Wallet.class), Mockito.anyBoolean()))
                .thenReturn(new ArrayList<Transaction>());
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        transactionService.importTransactions(walletsInfo);
        // Then
        Mockito.verify(transactionRepository, Mockito.times(1)).saveAll(Mockito.anyList());
    }

    @Test
    void makeNewTransaction() {
        // Given
        List<Transaction> expectedTransactions = MockDataProvider.getTransactionPairs(null, null);
        Transaction newTransaction = MockDataProvider.getTransactionCopy(expectedTransactions.get(0));
        newTransaction.setReason(null);
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        List<Transaction> actualTransactions = transactionService.makeNewTransaction(newTransaction, newTransaction.getCategory(), outgoingWallet, incomingWallet);
        // Then
        assertEquals(expectedTransactions, actualTransactions);
    }

    @Test
    void makeNewInternalTransaction() {
        // Given
        List<Transaction> expectedTransactions = MockDataProvider.getTransactionPairs(null, null);
        expectedTransactions.forEach(transaction -> transaction.setCategory(TransactionCategory.INTERNAL));
        Transaction newTransaction = MockDataProvider.getTransactionCopy(expectedTransactions.get(0));
        newTransaction.setReason(null);
        Mockito.when(walletService.findWalletById(1L)).thenReturn(outgoingWallet);
        Mockito.when(walletService.findWalletById(2L)).thenReturn(incomingWallet);
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        List<Transaction> actualTransactions = transactionService.makeNewInternalTransaction(newTransaction, 1L, 2L);
        // Then
        assertEquals(expectedTransactions, actualTransactions);
    }

    @Test
    void makeNewEarningTransaction() {
        // Given
        List<Transaction> expectedTransactions = MockDataProvider.getTransactionPairs(null, null);
        expectedTransactions.forEach(transaction -> transaction.setCategory(TransactionCategory.EARNING));
        Transaction newTransaction = MockDataProvider.getTransactionCopy(expectedTransactions.get(0));
        newTransaction.setReason(null);
        Mockito.when(walletService.findWalletByName("Earning")).thenReturn(outgoingWallet);
        Mockito.when(walletService.findWalletByName("Unallocated")).thenReturn(incomingWallet);
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        List<Transaction> actualTransactions = transactionService.makeNewEarningTransaction(newTransaction);
        // Then
        assertEquals(expectedTransactions, actualTransactions);
    }

    @Test
    void makeNewSpendingTransaction() {
        // Given
        List<Transaction> expectedTransactions = MockDataProvider.getTransactionPairs(null, null);
        expectedTransactions.forEach(transaction -> transaction.setCategory(TransactionCategory.SPENDING));
        Transaction newTransaction = MockDataProvider.getTransactionCopy(expectedTransactions.get(0));
        newTransaction.setReason(null);
        Mockito.when(walletService.findWalletById(1L)).thenReturn(outgoingWallet);
        Mockito.when(walletService.findWalletByName("Spending")).thenReturn(incomingWallet);
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        List<Transaction> actualTransactions = transactionService.makeNewSpendingTransaction(newTransaction, 1L);
        // Then
        assertEquals(expectedTransactions, actualTransactions);
    }

    @Test
    void canGetTransaction() {
        // Given
        Long transactionId = 1L;
        Transaction expectedTransaction = new Transaction(1L, outgoingWallet, 1000L, LocalDate.now(), LocalTime.now(), "Can You Get Me", TransactionType.OUTFLOW, TransactionCategory.SPENDING);
        Mockito.when(transactionRepository.findById(transactionId)).thenReturn(java.util.Optional.of(expectedTransaction));
        // When
        Transaction actualTransaction = transactionService.getTransaction(transactionId);
        // Then
        assertEquals(expectedTransaction, actualTransaction);
    }

    @Test
    void canNotGetTransaction() {
        // Given
        Long transactionId = 1L;
        String expectedMessage = String.format("transaction with id %s was not found", transactionId);
        // When
        String actualMessage = assertThrows(IllegalStateException.class, () -> transactionService.getTransaction(transactionId)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void getTransactionPair() {
        // Given
        Long transactionId = 1L;
        Long transactionPairId = 2L;
        List<Transaction> expectedTransactions = MockDataProvider.getTransactionPairs(transactionId, transactionPairId);
        Transaction expectedTransactionPair = expectedTransactions.get(1);
        Mockito.when(transactionRepository.findById(transactionId)).thenReturn(java.util.Optional.ofNullable(expectedTransactions.get(0)));
        // When
        Transaction actualTransactionPair = transactionService.getTransactionPair(transactionId);
        // Then
        assertEquals(expectedTransactionPair, actualTransactionPair);
    }

    @Test
    void getWalletTransactionBalance() {
        // Given
        Transaction transaction1 = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, outgoingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction5 = new Transaction(5L, outgoingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<Transaction> outgoingWalletTransactions = List.of(transaction1, transaction3, transaction5);
        Long expectedTransactionBalance = transaction1.getEvaluatedAmount() +
                transaction3.getEvaluatedAmount() + transaction5.getEvaluatedAmount();
        Mockito.when(transactionRepository.findAllByAssociatedWalletAndIsArchived(outgoingWallet, false)).thenReturn(outgoingWalletTransactions);
        // When
        Long actualTransactionBalance = transactionService.getWalletTransactionBalance(outgoingWallet);
        // Then
        assertEquals(expectedTransactionBalance, actualTransactionBalance);
    }

    @Test
    void findWalletTransactions() {
        // Given
        Transaction transaction1 = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, outgoingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction5 = new Transaction(5L, outgoingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<Transaction> expectedWalletTransactions = List.of(transaction1, transaction3, transaction5);
        Mockito.when(transactionRepository.findAllByAssociatedWalletAndIsArchived(outgoingWallet, false)).thenReturn(List.of(transaction1, transaction3, transaction5));
        // When
        List<Transaction> actualWalletTransactions = transactionService.findWalletTransactions(outgoingWallet);
        // Then
        assertEquals(expectedWalletTransactions, actualWalletTransactions);
    }

    @Test
    void deleteTransaction() {
        // Given
        Long transactionId = 1L;
        Long transactionPairId = 2L;
        List<Transaction> transactionsToBeDeleted = MockDataProvider.getTransactionPairs(transactionId, transactionPairId);
        Transaction transactionToBeDeleted = transactionsToBeDeleted.get(0);
        Mockito.when(transactionRepository.findById(transactionId)).thenReturn(java.util.Optional.of(transactionToBeDeleted));
        Mockito.doNothing().when(transactionTemplateService).deleteTemplateIfExists(Mockito.any(Transaction.class));
        Mockito.when(transactionRepository.save(transactionToBeDeleted)).thenAnswer(i -> i.getArgument(0));
        // When
        transactionService.deleteTransaction(transactionId);
        // Then
        Mockito.verify(transactionTemplateService, Mockito.times(2))
                .deleteTemplateIfExists(Mockito.any(Transaction.class));
    }

    @Test
    void deleteWalletTransactions() {
        // Given
        Long walletId = outgoingWallet.getId();
        Transaction transaction1 = MockDataProvider.getTransactionPairs(1L, 2L).get(0);
        Transaction transaction3 = MockDataProvider.getTransactionPairs(3L, 4L).get(0);
        transaction3.setDate(transaction3.getDate().plusDays(1));
        Transaction transaction5 = MockDataProvider.getTransactionPairs(5L, 6L).get(0);
        transaction5.setDate(transaction5.getDate().plusDays(2));
        List<Transaction> transactionsToBeDeleted = List.of(transaction1, transaction3, transaction5);
        Mockito.when(walletService.findWalletById(walletId)).thenReturn(outgoingWallet);
        Mockito.when(transactionRepository.findAllByAssociatedWalletAndIsArchived(outgoingWallet, false)).thenReturn(transactionsToBeDeleted);
        Mockito.doNothing().when(transactionTemplateService).deleteTemplateIfExists(Mockito.any(Transaction.class));
        Mockito.when(transactionRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        transactionService.deleteWalletTransactions(walletId);
        // Then
        Mockito.verify(transactionTemplateService, Mockito.times(transactionsToBeDeleted.size()))
                .deleteTemplateIfExists(Mockito.any(Transaction.class));
    }

    @Test
    void findLastWalletTransaction() {
        // Given
        Transaction transaction1 = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(3L, outgoingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction expectedLastTransaction = new Transaction(5L, outgoingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<Transaction> outgoingWalletTransactions = List.of(transaction1, transaction3, expectedLastTransaction);
        Mockito.when(transactionRepository.findAllByAssociatedWalletAndIsArchived(outgoingWallet, false)).thenReturn(outgoingWalletTransactions);
        // When
        Transaction actualLastTransaction = transactionService.findLastWalletTransaction(outgoingWallet);
        // Then
        assertEquals(expectedLastTransaction, actualLastTransaction);
    }
}
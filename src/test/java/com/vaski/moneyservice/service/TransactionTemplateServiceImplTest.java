package com.vaski.moneyservice.service;

import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.models.*;
import com.vaski.moneyservice.repository.TransactionTemplateRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTemplateServiceImplTest {

    @Mock
    private TransactionTemplateRepository transactionTemplateRepository;

    private TransactionTemplateServiceImpl transactionTemplateService;

    private Wallet outgoingWallet;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transactionTemplateService = new TransactionTemplateServiceImpl(transactionTemplateRepository);
        outgoingWallet = MockDataProvider.getNormalWallet(1L);
    }

    @Test
    void createNewTemplate() {
        // Given
        Transaction transactionForTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Mockito.when(transactionTemplateRepository.save(Mockito.any(TransactionTemplate.class)))
                .thenAnswer(i -> i.getArgument(0));
        // When
        transactionTemplateService.createNewTemplate(transactionForTemplate);
        // Then
        Mockito.verify(transactionTemplateRepository, Mockito.times(1))
                .save(Mockito.any(TransactionTemplate.class));
    }

    @Test
    void getAllTemplates() {
        // Given
        Transaction transaction1 = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction2 = new Transaction(3L, outgoingWallet, 100L, LocalDate.of(2021, 1, 16), LocalTime.of(10, 10), "Test reason 2", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Transaction transaction3 = new Transaction(5L, outgoingWallet, 100L, LocalDate.of(2021, 1, 17), LocalTime.of(10, 10), "Test reason 3", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        List<TransactionTemplate> expectedTemplates = List.of(
                new TransactionTemplate(transaction1),
                new TransactionTemplate(transaction2),
                new TransactionTemplate(transaction3));
        Mockito.when(transactionTemplateRepository.findAll()).thenReturn(List.of(
                new TransactionTemplate(transaction1),
                new TransactionTemplate(transaction2),
                new TransactionTemplate(transaction3)));
        // When
        List<TransactionTemplate> actualTemplates = transactionTemplateService.getAllTemplates();
        // Then
        assertEquals(expectedTemplates, actualTemplates);
    }

    @Test
    void getTemplateById() {
        // Given
        Long transactionTemplateId = 1L;
        Transaction transactionForTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        TransactionTemplate expectedTemplate = new TransactionTemplate(transactionTemplateId, transactionForTemplate);
        Mockito.when(transactionTemplateRepository.findById(transactionTemplateId))
                .thenAnswer(i -> java.util.Optional.of(new TransactionTemplate(i.getArgument(0), transactionForTemplate)));
        // When
        TransactionTemplate actualTemplate = transactionTemplateService.getTemplateById(transactionTemplateId);
        // Then
        assertEquals(expectedTemplate, actualTemplate);
    }

    @Test
    void canNotGetTemplateById() {
        // Given
        Long transactionTemplateId = 1L;
        String expectedMessage = String.format("transaction template with id %s was not found", transactionTemplateId);
        // When
        String actualMessage = assertThrows(IllegalStateException.class, () -> transactionTemplateService.getTemplateById(transactionTemplateId)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void getTemplateByTransaction() {
        // Given
        Long transactionTemplateId = 1L;
        Transaction transactionToFindTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        TransactionTemplate expectedTemplate = new TransactionTemplate(transactionTemplateId, transactionToFindTemplate);
        Mockito.when(transactionTemplateRepository.findByTransaction(transactionToFindTemplate))
                .thenAnswer(i -> Optional.of(new TransactionTemplate(transactionTemplateId, i.getArgument(0))));
        // When
        TransactionTemplate actualTemplate = transactionTemplateService.getTemplateByTransaction(transactionToFindTemplate);
        // Then
        assertEquals(expectedTemplate, actualTemplate);
    }

    @Test
    void canNotGetTemplateByTransaction() {
        // Given
        Transaction transactionToFindTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        // When
        TransactionTemplate actualTemplate = transactionTemplateService.getTemplateByTransaction(transactionToFindTemplate);
        // Then
        assertNull(actualTemplate);
    }

    @Test
    void deleteTemplateIfExistsByTransaction() {
        // Given
        Long transactionTemplateId = 1L;
        Transaction transactionToDeleteTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        Mockito.when(transactionTemplateRepository.findByTransaction(transactionToDeleteTemplate))
                .thenAnswer(i -> Optional.of(new TransactionTemplate(transactionTemplateId, i.getArgument(0))));
        Mockito.doNothing().when(transactionTemplateRepository).delete(Mockito.any(TransactionTemplate.class));
        // When
        transactionTemplateService.deleteTemplateIfExists(transactionToDeleteTemplate);
        // Then
        Mockito.verify(transactionTemplateRepository, Mockito.times(1))
                .delete(Mockito.any(TransactionTemplate.class));
    }

    @Test
    void canNotDeleteTemplateIfExistsByTransaction() {
        // Given
        Transaction transactionToDeleteTemplate = new Transaction(1L, outgoingWallet, 100L, LocalDate.of(2021, 1, 15), LocalTime.of(10, 10), "Test reason 1", TransactionType.OUTFLOW, TransactionCategory.INTERNAL);
        // When
        transactionTemplateService.deleteTemplateIfExists(transactionToDeleteTemplate);
        // Then
        Mockito.verify(transactionTemplateRepository, Mockito.times(0))
                .delete(Mockito.any(TransactionTemplate.class));
    }

    @Test
    void deleteTemplateIfExistsById() {
        // Given
        Long transactionTemplateId = 1L;
        Mockito.doNothing().when(transactionTemplateRepository).deleteById(transactionTemplateId);
        // When
        transactionTemplateService.deleteTemplateIfExists(transactionTemplateId);
        // Then
        Mockito.verify(transactionTemplateRepository, Mockito.times(1))
                .deleteById(transactionTemplateId);
    }
}
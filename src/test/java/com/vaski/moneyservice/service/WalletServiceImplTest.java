package com.vaski.moneyservice.service;

import com.vaski.moneyservice.dataprovider.MockDataProvider;
import com.vaski.moneyservice.domain.exception.RecordAlreadyExistException;
import com.vaski.moneyservice.domain.models.Transaction;
import com.vaski.moneyservice.domain.models.Wallet;
import com.vaski.moneyservice.repository.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class WalletServiceImplTest {

    @Mock
    private WalletRepository walletRepository;

    private WalletServiceImpl walletService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        walletService = new WalletServiceImpl(walletRepository);
    }

    @Test
    void importWallets() {
        // Given
        Wallet wallet1 = MockDataProvider.getNormalWallet(1L);
        Wallet wallet2 = MockDataProvider.getNormalWallet(2L);
        Wallet wallet3 = MockDataProvider.getNormalWallet(3L);
        Wallet wallet4 = MockDataProvider.getNormalWallet(4L);
        Wallet wallet5 = MockDataProvider.getNormalWallet(5L);
        Map<Wallet, List<Transaction>> givenWalletsInfo =
                Map.of(wallet1, new ArrayList<>(), wallet2, new ArrayList<>(), wallet3, new ArrayList<>(), wallet4, new ArrayList<>(), wallet5, new ArrayList<>());
        List<Wallet> previousWalletState = List.of(wallet1, wallet2, wallet3);
        Mockito.when(walletRepository.findAllByIsArchived(false)).thenReturn(previousWalletState);
        Mockito.when(walletRepository.saveAll(Mockito.anyList())).thenAnswer(i -> i.getArgument(0));
        // When
        walletService.importWallets(givenWalletsInfo);
        // Then
        Mockito.verify(walletRepository, Mockito.times(1)).findAllByIsArchived(false);
        Mockito.verify(walletRepository, Mockito.times(1)).saveAll(Mockito.anyList());
    }

    @Test
    void createNewWallet() {
        // Given
        Wallet walletToBeCreated = MockDataProvider.getNormalWallet(1L);
        Mockito.when(walletRepository.existsByNameAndIsArchived(walletToBeCreated.getName(), false)).thenReturn(false);
        Mockito.when(walletRepository.save(Mockito.any(Wallet.class))).thenAnswer(i -> i.getArgument(0));
        // When
        Wallet actualWallet = walletService.createNewWallet(walletToBeCreated);
        // Then
        assertEquals(walletToBeCreated, actualWallet);
    }

    @Test
    void canNotCreateNewWallet() {
        // Given
        Wallet walletToBeCreated = MockDataProvider.getNormalWallet(1L);
        Mockito.when(walletRepository.existsByNameAndIsArchived(walletToBeCreated.getName(), false)).thenReturn(true);
        String expectedMessage = String.format("wallet with name %s already exists", walletToBeCreated.getName());
        // When
        String actualMessage = assertThrows(RecordAlreadyExistException.class, () -> walletService.createNewWallet(walletToBeCreated)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void findWalletById() {
        // Given
        Long walletId = 1L;
        Wallet expectedWallet = MockDataProvider.getNormalWallet(walletId);
        Mockito.when(walletRepository.findById(walletId)).thenReturn(Optional.of(MockDataProvider.getNormalWallet(walletId)));
        // When
        Wallet actualWallet = walletService.findWalletById(walletId);
        // Then
        assertEquals(expectedWallet, actualWallet);
    }

    @Test
    void canNotFindWalletById() {
        // Given
        Long walletId = 1L;
        String expectedMessage = String.format("wallet with id %s was not found", walletId);
        // When
        String actualMessage = assertThrows(IllegalStateException.class, () -> walletService.findWalletById(walletId)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void findAllWallets() {
        // Given
        Wallet wallet1 = MockDataProvider.getNormalWallet(1L);
        Wallet wallet2 = MockDataProvider.getNormalWallet(2L);
        Wallet wallet3 = MockDataProvider.getNormalWallet(3L);
        List<Wallet> expectedWallets = List.of(wallet1, wallet2, wallet3);
        Mockito.when(walletRepository.findAllByIsArchived(false)).thenReturn(expectedWallets);
        // When
        List<Wallet> actualWallets = walletService.findAllWallets();
        // Then
        assertEquals(expectedWallets, actualWallets);
    }

    @Test
    void findWalletByName() {
        // Given
        String walletName = "Wallet 1";
        Wallet expectedWallet = MockDataProvider.getNormalWallet(1L);
        Mockito.when(walletRepository.findWalletByName(walletName))
                .thenAnswer(i -> Optional.of(MockDataProvider.getNormalWallet(1L, i.getArgument(0))));
        // When
        Wallet actualWallet = walletService.findWalletByName(walletName);
        // Then
        assertEquals(expectedWallet, actualWallet);
    }


    @Test
    void canNotFindWalletByName() {
        // Given
        String walletName = "Wallet 1";
        String expectedMessage = String.format("wallet with name %s was not found", walletName);
        // When
        String actualMessage = assertThrows(IllegalStateException.class, () -> walletService.findWalletByName(walletName)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void deleteWallet() {
        // Given
        Long walletId = 1L;
        Wallet walletToBeDeleted = MockDataProvider.getNormalWallet(walletId);
        Mockito.when(walletRepository.findById(walletId)).thenReturn(Optional.of(walletToBeDeleted));
        Mockito.when(walletRepository.save(walletToBeDeleted)).thenReturn(null);
        // When
        walletService.deleteWallet(walletId);
        // Then
        Mockito.verify(walletRepository, Mockito.times(1))
                .findById(walletId);
        Mockito.verify(walletRepository, Mockito.times(1))
                .save(walletToBeDeleted);
    }


    @Test
    void archiveWallet() {
        // Given
        Long walletId = 1L;
        Wallet walletToBeDeleted = MockDataProvider.getNormalWallet(walletId);
        Mockito.when(walletRepository.findById(walletId)).thenReturn(Optional.of(walletToBeDeleted));
        Mockito.when(walletRepository.save(walletToBeDeleted)).thenReturn(null);
        // When
        walletService.archiveWallet(walletId);
        // Then
        Mockito.verify(walletRepository, Mockito.times(1))
                .findById(walletId);
        Mockito.verify(walletRepository, Mockito.times(1))
                .save(walletToBeDeleted);
    }

    @Test
    void canNotArchiveWallet() {
        // Given
        Long walletId = 1L;
        Wallet walletToBeArchived = MockDataProvider.getSystemWallet(walletId, "System wallet");
        Mockito.when(walletRepository.findById(walletId)).thenReturn(Optional.of(walletToBeArchived));
        String expectedMessage = "you can not delete system wallet";
        // When
        String actualMessage = assertThrows(IllegalStateException.class, () -> walletService.archiveWallet(walletId)).getMessage();
        // Then
        assertEquals(expectedMessage, actualMessage);
    }

}